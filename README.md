# Python

Python is an interpreted, object oriented programming language. Can be used to build anything with right set of tools and libraries.

### Arrays Methods

List are called the arrays in python. There are 11 methods to perform on a list in python. To initalize any variable as a list we can use **_square brackets [] or list()_**. List can contain same data type elements or mix data type elements. **list()** is also used for **type conversions**.

- **Append**

    ```python
    # Syntax
    list.append(element_u_want_to_append)
    ```

    Append method is used to append an element in the end of the list.

    ```python
    list_of_items = [1, "Harsh", 99]
    list_of_items.append(45)
    list_of_items
    ```
    We can add two or more list into one using + add symbol.

    ```python
    list_of_items = [1, "Harsh", 99]
    list_of_fruits = ["Apple","Mango"]
    combined = list_of_items + list_of_fruits
    combined
    ```

- **Clear**

    To remove all the items present in the list or to empty a list.

    ```python
    # Syntax
    list.clear()
    ```

    ```python
    checklist = ['Wake up', 'Workout', 'Office']
    checklist.clear()
    checklist
    ```
- **Copy**

    To copy all the elements of the list to a variable or to a list.

    ```python
    # Syntax
    list.copy()
    ```

    ```python
    checklist = ['Jog','Office','Skipping']
    todo = checklist.copy()
    todo
    ```
- **Count**

    To know about the occurence of a particular element in a list we use count method.

    ```python
    # Syntax
    list.count(element_that_you_want_to_search)
    ```

    ```python
    mylist = ['Harsh', 'Pranshu', 'Aman','Harsh']
    mylist.count('Harsh')
    ```

- **Extend**

    To extend or append a list with another list we use extend method. The elements of another list gets appended at the end of the list.

    ```python
    # Syntax
    mylist.extend(another_list)
    ```

    ```python
    myList = [1, 2, 3, 4]
    anotherList = [5, 6, 7]
    myList.extend(anotherList)
    myList
    ```

- **Index**

    Index method is used to find the index of the element we want to search. Note that it will only give the index of the first occurence of that element in the list.

    ```python
    # Syntax
    lists.index(element_we_want_to_search)
    ```

    ```python
    myCars = ['Lamborgini', 'Ferrari', 'BMW', 'Volvo', 'BMW']
    myCars.index('BMW')
    ```

- **Insert**

    To insert an element at a particular index we use insert() method. We can pass the parameters like the index at which we want to append and the element we want to append at that particular index.

    ```python
    # Syntax
    my_list.insert(index, element)
    ```

    ```python
    my_list = ['BMW', 'Volvo', 'Dodge', 'Audi']
    my_list.insert(0,'Bugatti')
    ```

- **Pop**

    To remove and item from a list we use pop() method with index as a parameter.

    ```python
    # Syntax
    my_list.pop(index)
    ```

    ```python
    my_list = ['BMW', 'Volvo', 'Dodge', 'Audi']
    my_list.pop(2)
    ```

    Note that the pop() method can also be used without any parameter. In that case the element will pop like it happens in a stack data structure i.e. from the last.
    ```python
    my_list = ['Cars', 'Bike', 'Villa', 'Bungalow']
    my_list.pop()
    ```
- **Remove**

    Remove method is used to remove a element from a list. Note if there are multiple occurence of that element in the list then it is going to remove the first occurence of that element.

    ```python
    # Syntax
    my_list.remove(element)
    ```

    ```python
    my_list = ['Cars', 'Bike', 'Villa', 'Bungalow']
    my_list.remove('Villa')
    ```

    ```python
    # Example for multiple occurence
    my_cars = ['Lamborgini', 'BMW', 'Volvo', 'BMW']
    my_cars.remove('BMW')
    ```

- **Reverse**

    Reverse method is used to reverse the entire list

    ```python
    # Syntax
    my_list.reverse()
    ```

    ```python
    my_list = ['Repeat', 'Sleep', 'Eat', 'Office']
    my_list.reverse()
    ```

- **Sort**

    Sorting is used to sort the list according to the number order or from the lexiographical order.

    ```python
    # Syntax
    my_list.sort()
    ```

    ```python
    # Number order sorting
    my_list = [6, 1, 7, 8, 9]
    my_list.sort()
    my_list
    ```

    ```python
    # Lexicographically sorting
    my_list = ['Harshita', 'Harsh', 'Gaurav', 'Garima']
    my_list.sort()
    my_list
    ```

### String Methods

String are wrote using double quote (') (") and has **43 methods** to element to get the desired output. Some of the methods are:

- **Count**

    Count method is used to count the number of occurence of a value in a string. There are two other optional parameters in count method in which we can define the range from where till where we want to search our value.

    ```python 
    # Syntax
    string.count(value,start,end)
    ```

    ```python
    string = "I went to college in jalandhar pubjab and after that I have join my company in banglore."
    string.count('I')
    ```

    ```python
    string = "An apple a day keeps the doctor away. But I don't like apple."
    string.count('apple',10,20) # the 10 and 20 are the range the other 2 parameters.
    ```

- **Find**

    To find a particular value in a string we use find. It find the first occurence of the element or value that you're searching and return -1 if the value is not present. Same as count it also has start and end as 2 optional parameters.

    ```python
    # Syntax
    string.find(value, start, stop) 
    ```

    ```python
    string = "I went to college in punjab. My college name was LPU"
    string.find('LPU')
    ```

    ```python
    string = "This october I have joined the cohort 22 batch"
    string.find("cohort", 10, 20)
    ```

- **Index**

    To find the index of the particular element we use index() method it give the index of the first occurence of that element. It is similar to find method. It also have three parameter like the other methods in which 2 are optional. The index() method will give expection if the value is not present in the string. 

    ```python
    # Syntax
    string.index(element)
    ```

    ```python
    string = "Hello, Hi Comostas"
    string.index("Comostas") 
    ```

- **Isalnum**

    The method return true if the string contains only a-z or A-Z characters including 0 - 9 number. If the string contains special characters like '$','#', etc then it will return false cause it will only consider non-special characters.

    ```python
    # Syntax
    string.isalnum()
    ```

    ```python
    string = "Yes I can1"
    string.isalnum()
    ```

    ```python
    passcode = "Yes@!12"
    passcode.isalnum()
    ```
### Object and Object Oriented Programming 
